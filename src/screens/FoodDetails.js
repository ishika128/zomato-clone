
// import React  , {useState } from 'react';
// import { Text, View , StyleSheet  , StatusBar , ScrollView } from 'react-native';
// import Header from '../components/Header';
// import FoodCard from '../components/FoodCard';

// const FoodDetails = () => {

//   return (

//     <View style = {styles.container}>
//         <Header label ="All Collections "></Header>
//         <View></View>
//       <Text style={styles.textStyle}> Hello world </Text> 
      
     
         
//       </View>
    
//   )
// }
// const styles = StyleSheet.create({
//   textStyle:{
//     alignItems: "center",
//     fontSize:30 ,
//     marginTop:50 ,
//   } ,
//   container:{
    
//     flex :1 ,
//      alignItems: "center",
//     //justifyContent:'center',
//     // backgroundColor:'pink',

//   } ,
  
   

//   }) 

// export default FoodDetails;
import React  , {useState } from 'react';
import { Text, View , StyleSheet} from 'react-native';
import { ImageHeaderScrollView, TriggeringView } from 'react-native-image-header-scroll-view';
import DescriptionRadio from '../components/DescriptionRadio';

// Inside of a component's render() method:
const FoodDetails = () => {
  return (
    <ImageHeaderScrollView
      maxHeight={310}
      minHeight={100}
      headerImage={require("../images/buttercake.jpeg")}
    //   renderForeground={() => (
    //     <View style={{ height: 350, justifyContent: "center", alignItems: "center" }} >
    //       <TouchableOpacity onPress={() => console.log("tap!!")}>
    //         <Text style={{ backgroundColor: "transparent" }}>Tap Me!</Text>
    //       </TouchableOpacity>
    //     </View>
    //   )}
    >
       <View style={{ height: 1000 }}>
         <TriggeringView onHide={() => console.log("text hidden")}>
          <Text style ={styles.toptextstyle}>Butter Cake</Text>
          <Text style ={styles.toptextstyledetails}>Flavorful ,Soft and light in texture.Moist enough to stand on its own or to accommodate a variety of frostings and toppings.
          </Text>
          <View
  style={{
    borderBottomColor: 'black',
    borderBottomWidth: 0.5,
    marginTop:5
  }}
/>
{/* <Text style ={styles.toptextstyle}>Add Toppings ! </Text>
<Text style ={styles.toptextstyledetails}>You can choose any 1 option</Text> */}
<DescriptionRadio></DescriptionRadio>

        </TriggeringView> 
      </View> 
    </ImageHeaderScrollView>
  );
  
      }
      const styles = StyleSheet.create({
        toptextstyle :{
          fontWeight: '500',
          fontSize :21 ,
          marginLeft:14 , 
          marginTop: 7 ,
        },
        toptextstyledetails :{
          fontWeight: '400',
          fontSize :15 ,
          marginLeft:14 , 
          marginTop: 7 ,
          color:'grey',
        }
    
      })
      export default FoodDetails;