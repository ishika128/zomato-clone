import React  , {useState } from 'react';
import { Text, View , StyleSheet  , StatusBar , ScrollView } from 'react-native';
import Header from '../components/Header';
import FoodCard from '../components/FoodCard';

const HomeScreen = () => {

  return (

    <View style = {styles.container}>
        <Header label ="All Collections "></Header>
      {/* <Text style={styles.textStyle}> Hello world </Text> */}
      <ScrollView showsHorizontalScrollIndicator={false}>
      <FoodCard imglink = {require('../images/cake2.jpeg')} ></FoodCard>
       
       <FoodCard imglink = {require('../images/istockphoto-683734168-170667a.jpeg')}></FoodCard> 
      
      <FoodCard imglink = {require('../images/sandwich2.jpeg')}></FoodCard> 
      <FoodCard imglink = {require('../images/pizzza2.jpeg')}></FoodCard>
      <FoodCard imglink = {require('../images/icecream4.jpeg')}></FoodCard> 
      
      
      <StatusBar style ="auto "></StatusBar>
      </ScrollView>
     
         
      </View>
    
  )
}
const styles = StyleSheet.create({
  textStyle:{
    alignItems: "center",
    fontSize:30 ,
    marginTop:50 ,
  } ,
  container:{
    
    flex :1 ,
     alignItems: "center",
    //justifyContent:'center',
    // backgroundColor:'pink',

  } ,
  
   

  }) 

export default HomeScreen;