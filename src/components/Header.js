import React from "react";
import {View,Text,StyleSheet , Dimensions}from 'react-native';
const Header =(props)=>
{
    return(
        <View style = {styles.container}>
            <Text style = {styles.labelStyle}>{props.label}</Text>
        </View>
    );
}
const deviceWidth = Math.round(Dimensions .get('window').width ) ;
const styles = StyleSheet.create({
    container:{
width:deviceWidth,
height:62 ,
backgroundColor :'white' ,
justifyContent : 'flex-end', 
alignItems : 'center'
    } , 
    labelStyle : {
        fontSize : 18 , 
        fontWeight :'600' ,

    }
})
export default Header ;