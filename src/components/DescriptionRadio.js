import React from "react";
import { View, Text, StyleSheet, Dimensions, TextInput } from 'react-native';
import Tagc from "./Tagc";

const DescriptionRadio = (props) => {
    return (
        // <View>
        //      <Text style ={styles.toptextstyle}>Add Toppings ! </Text>

        //     <Text style ={styles.toptextstyledetails}>You can choose any 1 option</Text>
        // </View>
        <View>
            <View style={{ flexDirection: "row" }}>
                <View style={{ flex: 1 }}>
                    <Text style={styles.toptextstyle}  >Add Toppings ! </Text>
                  
                </View>
                <View style={{ flex: 1 }}>
                    <Text style={{ justifyContent: 'flex-start', marginTop : 8   }} >  <Tagc></Tagc></Text>
                </View>
            </View>
            <Text style ={styles.toptextstyledetails}>You can choose any 1 option</Text>
        </View>



    );
}

const styles = StyleSheet.create({
    toptextstyle :{
      fontWeight: '500',
      fontSize :21 ,
      marginLeft:14 , 
      marginTop: 7 ,
      justifyContent: 'flex-start', 
    },
    toptextstyledetails :{
      fontWeight: '400',
      fontSize :15 ,
      marginLeft:14 , 
      marginTop: 7 ,
      color:'grey',
    }

  })

export default DescriptionRadio;