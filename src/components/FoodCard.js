import React from 'react'
import { View, Text, StyleSheet, Dimensions, ImageBackground, TouchableOpacity, Image } from 'react-native'



const deviceWidth = Math.round(Dimensions.get('window').width);
const FoodCard = (props) => {
    const imL = props.imglink
    return (
        <View style={styles.cardContainer}>
            <ImageBackground source={props.imglink} opacity={0.9} style={styles.imageStyle}>
                <Text style={styles.textStyle}>Bakers make the World smell better.</Text>
                <Text style={styles.innertextStyle}>Let's have Bliss in Every Bite</Text>
                {/* <Text>{typeof props.imglink}</Text> */}
                {/* <View style ={styles.parentElement}> */}
                <View style={{flexDirection: 'row'}}>
                <Text style={styles.innertextStyle2}>5 Choices</Text>
                <TouchableOpacity  onPress={this.onBackClick}>
              
                    <Image style={styles.tinyLogo} source={{ uri: 'https://cdn-icons-png.flaticon.com/512/254/254434.png' }} />
                </TouchableOpacity>
                </View>
              
                {/* </View> */}



            </ImageBackground >

        </View>
    )
}

const styles = StyleSheet.create({

    cardContainer: {
        width: deviceWidth - 25,
        // backgroundColor : 'yellow',
        height: 200,
        borderRadius: 15,
        marginTop: 10
    },
    imageStyle: {
        height: 200,
        width: deviceWidth - 25
    },
    textStyle: {
        fontSize: 21,
        color: 'white',
        fontWeight: '500',
        marginTop: 15,
        marginLeft: 10,

    },
    innertextStyle: {
        fontSize: 15,
        color: 'white',
        fontWeight: '400',
        marginTop: 5,
        marginLeft: 10,

    },
    tinyLogo: {
        flex: 1,
        width: 10,
        height: 10,
        resizeMode: 'contain',
        tintColor: 'white',
        marginLeft:20,
        marginTop:100 
    
       
    },
    innertextStyle2: {
        fontSize: 15,
        color: 'white',
        fontWeight: '400',
        marginTop: 100,
        marginLeft: 10,
        alignItems : 'flex-start'

    }

})

export default FoodCard
